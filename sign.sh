#!/bin/bash

certs="/etc/letsencrypt/live/backend.chorechampion.com-0002"

openssl smime \
    -sign \
    -signer ${certs}/cert.pem \
    -inkey ${certs}/privkey.pem \
    -certfile ${certs}/chain.pem \
    -nodetach \
    -outform der \
    -in sa.mobileconfig \
    -out sa-signed.mobileconfig
